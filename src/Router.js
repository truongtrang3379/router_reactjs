import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Menu from './component/Menu'
import router from './data/router'
class Routerr extends React.Component {
    
    render() {
        
        return (
            <Router>
                <div>
                    <Menu />
                </div>
                <Switch>
                    {this.showContentMenu(router)}
                </Switch>
            </Router>

        )
    }
    

    showContentMenu = (router) => {
        var result = null;
        if (router.length > 0) {
            
            result = router.map((data,index) => {
                
                return (
                <Route
                    key={index}
                    path={data.path}
                    exact={data.exact}
                    component={data.main}
                />)
            })
        }
        return result;
    }
}
export default Routerr;
