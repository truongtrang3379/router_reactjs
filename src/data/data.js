const data = [
    {
        name: 'Samsung Galaxy A9',
        slug:"samsung_galaxy_A9",
        img: "https://cdn.tgdd.vn/Products/Images/42/192003/samsung-galaxy-a9-2018-blue-400x460.png",
        price: 9490000,
        id: 1,
    },
    {
        name: 'OPPO F11',
        slug:"OPPO_F11",
        img: "https://cdn.tgdd.vn/Products/Images/42/199801/oppo-f11-mtp-400x460.png",
        price: 7290000,
        id: 2,
    },
    {
        name: 'OPPO _F11_Pro_128GB',
        slug:"OPPO_F11_Pro_128GB",
        img: "https://cdn.tgdd.vn/Products/Images/42/202704/oppo-f11-pro-xam-128gb-docquyen-400x460.png",
        price: 8490000,
        id: 3,
    },
    {
        name: 'iPhone Xs 256GB',
        slug:"iPhone_Xs_256GB",
        img: "https://cdn.tgdd.vn/Products/Images/42/190324/iphone-xs-256gb-white-400x460.png",
        price: 32990000,
        id: 4,
    },
    {
        name: 'Samsung Galaxy S10+',
        slug:"samsung_galaxy_S10+",
        img: "https://cdn.tgdd.vn/Products/Images/42/198986/samsung-galaxy-s10-plus-512gb-ceramic-black-400x460.png",
        price: 28990000,
        id: 5,
    },
    {
        name: 'Samsung Galaxy A70',
        slug:"samsung_galaxy_A70",
        img: "https://cdn.tgdd.vn/Products/Images/42/195012/samsung-galaxy-a70-black-400x460.png",
        price: 9290000,
        id: 6,
    },
    {
        name: 'Samsung Galaxy A10',
        slug:"samsung_galaxy_A10",
        img: "https://cdn.tgdd.vn/Products/Images/42/197512/samsung-galaxy-a10-red-400x460.png",
        price: 3090000,
        id: 7,
    },
    {
        name: 'Realme C2 16GB',
        slug:"Realme_C2_16GB",
        img: "https://cdn.tgdd.vn/Products/Images/42/203347/realme-c2-16gb-blue-400x460.png",
        price: 2790000,
        id: 8,
    },
    {
        name: 'iPhone Xr 128GB',
        slug:"iPhone_Xr_128GB",
        img: "https://cdn.tgdd.vn/Products/Images/42/191483/iphone-xr-128gb-red-400x460.png",
        price: 21990000,
        id: 9,
    },
    {
        name: 'iPhone Xr 64GB',
        slug:"iPhone_Xr_64GB",
        img: "https://cdn.tgdd.vn/Products/Images/42/190325/iphone-xr-black-400x460.png",
        price: 17990000,
        id: 10,
    },
    {
        name: 'Huawei P30 Pro',
        slug:"huawei_P30_Pro",
        img: "https://cdn.tgdd.vn/Products/Images/42/197228/huawei-p30-pro-1-400x460.png",
        price: 22990000,
        id: 11,
    },
    {
        name: 'Samsung Galaxy Note 9',
        slug:"samsung_galaxy_Note9",
        img: "https://cdn.tgdd.vn/Products/Images/42/154897/samsung-galaxy-note-9-black-400x460-400x460.png",
        price: 19990000,
        id: 12,
    }
]
export default data;
