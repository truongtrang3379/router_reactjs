import React from 'react';
import Home from '../component/Home'
import Product from "../component/Product"
import Notfound from '../component/Notfound'
const routes = [
    {
        path:'/',
        exact: true,
        main : ({match})=><Home match={match}/>
    },
    {
        path:'/product',
        exact: false,
        main : ()=><Product/>
    },
    {
        path:'',
        exact: false,
        main : ()=><Notfound/>
    },
]
export default routes;