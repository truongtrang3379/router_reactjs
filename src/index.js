import React from 'react';
import ReactDOM from 'react-dom';
import Routerr from './Router';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Routerr/>, document.getElementById('root'));
serviceWorker.unregister();
