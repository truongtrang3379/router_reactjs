import React from 'react';
import { Link, Route } from 'react-router-dom';
const Menulink = ({ lable, to, activeOnlyWhenExact }) => {
    return (
        <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => {
            var active = match ? 'active' : '';
            return (
                <li className={active}>
                    <Link to={to}>{lable}</Link>
                </li>
            )
        }}
        />
    )
}
class Menu extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-inverse">
                <ul className="nav navbar-nav">
                    <Menulink lable="Sản phẩm" to='/' activeOnlyWhenExact={true} />
                    <Menulink lable="Giỏ hàng" to='/product' />
                </ul>
            </nav>
        )
    }
}
export default Menu;
