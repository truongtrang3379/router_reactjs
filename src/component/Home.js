import React from 'react';
import data from '../data/data'
import { NavLink } from 'react-router-dom'
class Home extends React.Component {
    render() {
       let {match}= this.props
    //    let url = match.url
       console.log(match)
        let loadData = data.map((data) => {
            return (
                <NavLink to={`${data.slug}`} key={data.id} >
                    <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3" >
                        <div className="thumbnail">
                            <img
                                src={data.img}
                                alt={data.name} />
                            <div className="caption">
                                <h3 title={data.name} >{data.name}</h3>
                                <p>
                                    {Number(data.price).toLocaleString()}
                                </p>
                                <p>
                                    <button className="btn btn-primary">Mua ngay</button>
                                </p>
                            </div>
                        </div>

                    </div>
                </NavLink>
            )
        })
        return (
            <div className="container">
                {loadData}
            </div>

        )
    }
}
export default Home;
